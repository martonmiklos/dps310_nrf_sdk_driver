/**
* Arduino library to control Dps310
*
* "Dps310" represents Infineon's high-sensetive pressure and temperature sensor.
* It measures in ranges of 300 - 1200 hPa and -40 and 85 °C.
* The sensor can be connected via SPI or I2C.
* It is able to perform single measurements
* or to perform continuous measurements of temperature and pressure at the same time,
* and stores the results in a FIFO to reduce bus communication.
*
* Have a look at the datasheet for more information.
*/

#ifndef DPS310_ARDUINO_H_INCLUDED
#define DPS310_ARDUINO_H_INCLUDED

#include "nrf_delay.h"
#include "nrf_drv_twi.h"
#include "app_error.h"

#define delay(x) {nrf_delay_ms(x);}

#include "dps310_consts.h"

#include <stdint.h>

//Idle Mode
int16_t dps310_standby(void);

//Command Mode
int16_t dps310_measureTempOnce(int32_t *result, uint8_t oversamplingRate);
int16_t dps310_startMeasureTempOnce(uint8_t oversamplingRate);
int16_t dps310_measurePressureOnce(int32_t *result, uint8_t oversamplingRate);
int16_t dps310_startMeasurePressureOnce(uint8_t oversamplingRate);
int16_t dps310_getSingleResult(int32_t *result);

//Background Mode
int16_t dps310_startMeasureTempCont(uint8_t measureRate, uint8_t oversamplingRate);
int16_t dps310_startMeasurePressureCont(uint8_t measureRate, uint8_t oversamplingRate);
int16_t dps310_startMeasureBothCont(uint8_t tempMr, uint8_t tempOsr, uint8_t prsMr, uint8_t prsOsr);
int16_t dps310_getContResults(int32_t *tempBuffer, uint8_t *tempCount, int32_t *prsBuffer, uint8_t *prsCount);

//Interrupt Control
int16_t dps310_setInterruptPolarity(uint8_t polarity);
int16_t dps310_setInterruptSources(uint8_t fifoFull, uint8_t tempReady, uint8_t prsReady);
int16_t dps310_getIntStatusFifoFull(void);
int16_t dps310_getIntStatusTempReady(void);
int16_t dps310_getIntStatusPrsReady(void);

//function to fix a hardware problem on some devices
int16_t dps310_correctTemp(void);


//enum for operating mode
typedef enum
{
    IDLE=0x00,
    CMD_PRS=0x01,
    CMD_TEMP=0x02,
    INVAL_OP_CMD_BOTH=0x03,		//invalid
    INVAL_OP_CONT_NONE=0x04, 	//invalid
    CONT_PRS=0x05,
    CONT_TMP=0x06,
    CONT_BOTH=0x07
} Mode;


//measurement
void dps310_init(nrf_drv_twi_t *twi, uint8_t slaveAddress);



#endif	//DPS310_ARDUINO_H_INCLUDED
